 ID: LL1_cotton_fiber_consumption_industry_dataset
 Name: Cotton_fiber_consumption_Industry
 Description: Cotton fiber, consumption INDUSTRY 
 License: open
 License Link: https://forecasters.org/resources/time-series-data/
 Source: International Institute of Forecasters; M-3 Competition Data
 Source Link: https://forecasters.org/resources/time-series-data/m3-competition/
 Citation: @article{makridakis2000m3, title={The M3-Competition: results, conclusions and implications}, author={Makridakis, Spyros and Hibon, Michele}, journal={International journal of forecasting}, volume={16}, number={4}, pages={451--476}, year={2000}, publisher={Elsevier}}
