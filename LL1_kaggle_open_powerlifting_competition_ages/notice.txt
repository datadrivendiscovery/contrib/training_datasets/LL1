 ID: LL1_kaggle_open_powerlifting_competition_ages_dataset
 Name: Open Powerlifting
 Description: This dataset is a snapshot of the OpenPowerlifting database as of February 2018. OpenPowerlifting is an organization which tracks meets and competitor results in the sport of powerlifting, in which competitors complete to lift the most weight for their class in three separate weightlifting categories.
 License: Creative Commons CC0 1.0 Universal (CC0 1.0)
 License Link: https://creativecommons.org/publicdomain/zero/1.0/
 Source: 
 Source Link: https://www.kaggle.com/open-powerlifting/powerlifting-database
 Citation: 
