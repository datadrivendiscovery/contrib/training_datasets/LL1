 ID: LL1_Lighting7_dataset
 Name: LL1_Lighting7
 Description: one of UCR datasets for timeseries classification
 License: open
 License Link: https://www.cs.ucr.edu/~eamonn/time_series_data/
 Source: UCR Time Series Classification Archive
 Source Link: https://www.cs.ucr.edu/~eamonn/time_series_data/
 Citation: @misc{UCRArchive, title={The UCR Time Series Classification Archive}, author={ Chen, Yanping and Keogh, Eamonn and Hu, Bing and Begum, Nurjahan and Bagnall, Anthony and Mueen, Abdullah and Batista, Gustavo}, year={2015}, month={July}, note = {\url{www.cs.ucr.edu/~eamonn/time_series_data/}}}
