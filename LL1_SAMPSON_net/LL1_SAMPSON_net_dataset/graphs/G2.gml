graph [
  directed 1
  multigraph 1
  name "()"
  node [
    id 0
    label "JHBSO"
    x 0.1301
    y 0.2215
    shape "0.5000"
  ]
  node [
    id 1
    label "BSL"
    x 0.1859
    y 0.9597
    shape "0.5000"
  ]
  node [
    id 2
    label "WNFI"
    x 0.3476
    y 0.2215
    shape "0.5000"
  ]
  node [
    id 3
    label "HG"
    x 0.2955
    y 0.1096
    shape "0.5000"
  ]
  node [
    id 4
    label "GEOY"
    x 0.2007
    y 0.4295
    shape "0.5000"
  ]
  node [
    id 5
    label "MR"
    x 0.4572
    y 0.4989
    shape "0.5000"
  ]
  node [
    id 6
    label "AAD"
    x 0.79
    y 0.7651
    shape "0.5000"
  ]
  node [
    id 7
    label "EIS"
    x 0.2156
    y 0.8054
    shape "0.5000"
  ]
  node [
    id 8
    label "SMLCU"
    x 0.329
    y 0.8076
    shape "0.5000"
  ]
  node [
    id 9
    label "PTR"
    x 0.8383
    y 0.217
    shape "0.5000"
  ]
  node [
    id 10
    label "LUS"
    x 0.8941
    y 0.0738
    shape "0.5000"
  ]
  node [
    id 11
    label "BNVNUE"
    x 0.9424
    y 0.3535
    shape "0.5000"
  ]
  node [
    id 12
    label "BRHL"
    x 0.7844
    y 0.2931
    shape "0.5000"
  ]
  node [
    id 13
    label "ABOE"
    x 0.7305
    y 0.3714
    shape "0.5000"
  ]
  node [
    id 14
    label "ABR"
    x 0.3086
    y 0.4228
    shape "0.5000"
  ]
  node [
    id 15
    label "VCO"
    x 0.7156
    y 0.2282
    shape "0.5000"
  ]
  node [
    id 16
    label "RMAD"
    x 0.7993
    y 0.443
    shape "0.5000"
  ]
  node [
    id 17
    label "BNFC"
    x 0.3216
    y 0.3445
    shape "0.5000"
  ]
  edge [
    source 0
    target 1
    key 0
    weight 1.0
  ]
  edge [
    source 0
    target 2
    key 0
    weight 1.0
  ]
  edge [
    source 0
    target 3
    key 0
    weight 1.0
  ]
  edge [
    source 1
    target 6
    key 0
    weight 1.0
  ]
  edge [
    source 1
    target 7
    key 0
    weight 1.0
  ]
  edge [
    source 1
    target 8
    key 0
    weight 1.0
  ]
  edge [
    source 2
    target 3
    key 0
    weight 1.0
  ]
  edge [
    source 3
    target 17
    key 0
    weight 1.0
  ]
  edge [
    source 4
    target 0
    key 0
    weight 1.0
  ]
  edge [
    source 4
    target 5
    key 0
    weight 1.0
  ]
  edge [
    source 4
    target 2
    key 0
    weight 1.0
  ]
  edge [
    source 5
    target 2
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 11
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 5
    key 0
    weight 1.0
  ]
  edge [
    source 6
    target 8
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 4
    key 0
    weight 1.0
  ]
  edge [
    source 7
    target 8
    key 0
    weight 1.0
  ]
  edge [
    source 8
    target 4
    key 0
    weight 1.0
  ]
  edge [
    source 9
    target 10
    key 0
    weight 1.0
  ]
  edge [
    source 9
    target 11
    key 0
    weight 1.0
  ]
  edge [
    source 9
    target 12
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 15
    key 0
    weight 1.0
  ]
  edge [
    source 10
    target 3
    key 0
    weight 1.0
  ]
  edge [
    source 11
    target 13
    key 0
    weight 1.0
  ]
  edge [
    source 11
    target 10
    key 0
    weight 1.0
  ]
  edge [
    source 12
    target 11
    key 0
    weight 1.0
  ]
  edge [
    source 12
    target 13
    key 0
    weight 1.0
  ]
  edge [
    source 13
    target 2
    key 0
    weight 1.0
  ]
  edge [
    source 14
    target 4
    key 0
    weight 1.0
  ]
  edge [
    source 14
    target 17
    key 0
    weight 1.0
  ]
  edge [
    source 15
    target 9
    key 0
    weight 1.0
  ]
  edge [
    source 15
    target 12
    key 0
    weight 1.0
  ]
  edge [
    source 15
    target 13
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 9
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 11
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 13
    key 0
    weight 1.0
  ]
  edge [
    source 16
    target 6
    key 0
    weight 1.0
  ]
  edge [
    source 17
    target 4
    key 0
    weight 1.0
  ]
  edge [
    source 17
    target 5
    key 0
    weight 1.0
  ]
  edge [
    source 17
    target 2
    key 0
    weight 1.0
  ]
]
