 ID: LL1_IzmenjavaBratSestra_net_dataset
 Name: IzmenjavaBratSestra Network
 Description: No description provided
 License: CC-NonCommercial-ShareAlike 2.5 License
 License Link: http://vlado.fmf.uni-lj.si/pub/networks/data/default.htm
 Source: Pajek
 Source Link: http://vlado.fmf.uni-lj.si/pub/networks/data/soc/Samo/Stranke94.htm
 Citation: @article{batagelj2006pajek, title={Pajek datasets [ol]}, author={Batagelj, Vladimir and Mrvar, Andrej}, journal={hup://vladojmJuni-lj. si/pub/networks/data}, year={2006}} @misc{batagelj2014pajek, title={Pajek.}, author={Batagelj, Vladimir and Mrvar, Andrej}, year={2014}}
