graph [
  directed 1
  multigraph 1
  node [
    id 0
    label "Algeria"
    x 0.4381
    y 0.4957
    shape "0.5000"
  ]
  node [
    id 1
    label "Argentina"
    x 0.2159
    y 0.7749
    shape "0.5000"
  ]
  node [
    id 2
    label "Australia"
    x 0.8636
    y 0.7009
    shape "0.5000"
  ]
  node [
    id 3
    label "Austria"
    x 0.4688
    y 0.4114
    shape "0.5000"
  ]
  node [
    id 4
    label "Barbados"
    x 0.267
    y 0.5766
    shape "0.5000"
  ]
  node [
    id 5
    label "Bangladesh"
    x 0.7301
    y 0.5071
    shape "0.5000"
  ]
  node [
    id 6
    label "Belgium /Lux."
    x 0.45
    y 0.3932
    shape "0.5000"
  ]
  node [
    id 7
    label "Belize"
    x 0.1352
    y 0.5259
    shape "0.5000"
  ]
  node [
    id 8
    label "Bolivia"
    x 0.2216
    y 0.6952
    shape "0.5000"
  ]
  node [
    id 9
    label "Brazil"
    x 0.2898
    y 0.6781
    shape "0.5000"
  ]
  node [
    id 10
    label "Canada"
    x 0.1364
    y 0.3305
    shape "0.5000"
  ]
  node [
    id 11
    label "Chile"
    x 0.1932
    y 0.7635
    shape "0.5000"
  ]
  node [
    id 12
    label "China"
    x 0.75
    y 0.4644
    shape "0.5000"
  ]
  node [
    id 13
    label "Colombia"
    x 0.2216
    y 0.5926
    shape "0.5000"
  ]
  node [
    id 14
    label "Croatia"
    x 0.4886
    y 0.4274
    shape "0.5000"
  ]
  node [
    id 15
    label "Cyprus"
    x 0.5142
    y 0.4701
    shape "0.5000"
  ]
  node [
    id 16
    label "Czech Rep."
    x 0.4773
    y 0.4017
    shape "0.5000"
  ]
  node [
    id 17
    label "Denmark"
    x 0.4602
    y 0.3761
    shape "0.5000"
  ]
  node [
    id 18
    label "Ecuador"
    x 0.1761
    y 0.6211
    shape "0.5000"
  ]
  node [
    id 19
    label "Egypt"
    x 0.517
    y 0.5071
    shape "0.5000"
  ]
  node [
    id 20
    label "El Salvador"
    x 0.1307
    y 0.5413
    shape "0.5000"
  ]
  node [
    id 21
    label "Fiji"
    x 0.9886
    y 0.6952
    shape "0.5000"
  ]
  node [
    id 22
    label "Finland"
    x 0.5114
    y 0.3305
    shape "0.5000"
  ]
  node [
    id 23
    label "France Mon."
    x 0.4375
    y 0.416
    shape "0.5000"
  ]
  node [
    id 24
    label "French Guiana"
    x 0.2784
    y 0.6097
    shape "0.5000"
  ]
  node [
    id 25
    label "Germany"
    x 0.4659
    y 0.3989
    shape "0.5000"
  ]
  node [
    id 26
    label "Greece"
    x 0.5028
    y 0.4501
    shape "0.5000"
  ]
  node [
    id 27
    label "Guadeloupe"
    x 0.2614
    y 0.5755
    shape "0.5000"
  ]
  node [
    id 28
    label "Guatemala"
    x 0.125
    y 0.5356
    shape "0.5000"
  ]
  node [
    id 29
    label "Honduras"
    x 0.1477
    y 0.5413
    shape "0.5000"
  ]
  node [
    id 30
    label "Hong Kong"
    x 0.8125
    y 0.51
    shape "0.5000"
  ]
  node [
    id 31
    label "Hungary"
    x 0.483
    y 0.4114
    shape "0.5000"
  ]
  node [
    id 32
    label "Iceland"
    x 0.3693
    y 0.3162
    shape "0.5000"
  ]
  node [
    id 33
    label "India"
    x 0.6932
    y 0.547
    shape "0.5000"
  ]
  node [
    id 34
    label "Indonesia"
    x 0.7869
    y 0.6211
    shape "0.5000"
  ]
  node [
    id 35
    label "Ireland"
    x 0.4119
    y 0.3647
    shape "0.5000"
  ]
  node [
    id 36
    label "Israel"
    x 0.5426
    y 0.4758
    shape "0.5000"
  ]
  node [
    id 37
    label "Italy"
    x 0.4716
    y 0.4359
    shape "0.5000"
  ]
  node [
    id 38
    label "Japan"
    x 0.875
    y 0.4587
    shape "0.5000"
  ]
  node [
    id 39
    label "Jordan"
    x 0.5568
    y 0.4815
    shape "0.5000"
  ]
  node [
    id 40
    label "Korea. Rep. Of"
    x 0.8438
    y 0.4615
    shape "0.5000"
  ]
  node [
    id 41
    label "Kuwait"
    x 0.5881
    y 0.4872
    shape "0.5000"
  ]
  node [
    id 42
    label "Latvia"
    x 0.5114
    y 0.3647
    shape "0.5000"
  ]
  node [
    id 43
    label "Madagascar"
    x 0.5852
    y 0.6838
    shape "0.5000"
  ]
  node [
    id 44
    label "Malaysia"
    x 0.804
    y 0.5869
    shape "0.5000"
  ]
  node [
    id 45
    label "Martinique"
    x 0.2642
    y 0.567
    shape "0.5000"
  ]
  node [
    id 46
    label "Mauritius"
    x 0.6165
    y 0.6963
    shape "0.5000"
  ]
  node [
    id 47
    label "Mexico"
    x 0.0795
    y 0.5014
    shape "0.5000"
  ]
  node [
    id 48
    label "Morocco"
    x 0.4091
    y 0.4729
    shape "0.5000"
  ]
  node [
    id 49
    label "Netherlands"
    x 0.4489
    y 0.3846
    shape "0.5000"
  ]
  node [
    id 50
    label "New Zealand"
    x 0.9602
    y 0.8091
    shape "0.5000"
  ]
  node [
    id 51
    label "Nicaragua"
    x 0.1477
    y 0.5527
    shape "0.5000"
  ]
  node [
    id 52
    label "Norway"
    x 0.4602
    y 0.3419
    shape "0.5000"
  ]
  node [
    id 53
    label "Oman"
    x 0.6193
    y 0.5214
    shape "0.5000"
  ]
  node [
    id 54
    label "Pakistan"
    x 0.6506
    y 0.4957
    shape "0.5000"
  ]
  node [
    id 55
    label "Panama"
    x 0.1761
    y 0.5755
    shape "0.5000"
  ]
  node [
    id 56
    label "Paraguay"
    x 0.2443
    y 0.7179
    shape "0.5000"
  ]
  node [
    id 57
    label "Peru"
    x 0.1818
    y 0.6667
    shape "0.5000"
  ]
  node [
    id 58
    label "Philippines"
    x 0.8352
    y 0.5499
    shape "0.5000"
  ]
  node [
    id 59
    label "Poland"
    x 0.4915
    y 0.3875
    shape "0.5000"
  ]
  node [
    id 60
    label "Portugal"
    x 0.4034
    y 0.4444
    shape "0.5000"
  ]
  node [
    id 61
    label "Moldava. Rep. Of"
    x 0.5114
    y 0.416
    shape "0.5000"
  ]
  node [
    id 62
    label "Reunion"
    x 0.6068
    y 0.7037
    shape "0.5000"
  ]
  node [
    id 63
    label "Romania"
    x 0.5057
    y 0.4274
    shape "0.5000"
  ]
  node [
    id 64
    label "Seychelles"
    x 0.6307
    y 0.6342
    shape "0.5000"
  ]
  node [
    id 65
    label "Singapore"
    x 0.7727
    y 0.5926
    shape "0.5000"
  ]
  node [
    id 66
    label "Slovenia"
    x 0.4716
    y 0.4217
    shape "0.5000"
  ]
  node [
    id 67
    label "Southern Africa"
    x 0.5085
    y 0.7179
    shape "0.5000"
  ]
  node [
    id 68
    label "Spain"
    x 0.4205
    y 0.4387
    shape "0.5000"
  ]
  node [
    id 69
    label "Sri Lanka"
    x 0.7017
    y 0.5726
    shape "0.5000"
  ]
  node [
    id 70
    label "Sweden"
    x 0.4801
    y 0.3533
    shape "0.5000"
  ]
  node [
    id 71
    label "Switzerland"
    x 0.4602
    y 0.4188
    shape "0.5000"
  ]
  node [
    id 72
    label "Thailand"
    x 0.7642
    y 0.5413
    shape "0.5000"
  ]
  node [
    id 73
    label "Trinidad Tobago"
    x 0.2614
    y 0.5869
    shape "0.5000"
  ]
  node [
    id 74
    label "Tunisia"
    x 0.4642
    y 0.4718
    shape "0.5000"
  ]
  node [
    id 75
    label "Turkey"
    x 0.5455
    y 0.4444
    shape "0.5000"
  ]
  node [
    id 76
    label "United Kingdom"
    x 0.429
    y 0.3732
    shape "0.5000"
  ]
  node [
    id 77
    label "United States"
    x 0.1193
    y 0.416
    shape "0.5000"
  ]
  node [
    id 78
    label "Uruguay"
    x 0.25
    y 0.7578
    shape "0.5000"
  ]
  node [
    id 79
    label "Venezuela"
    x 0.2273
    y 0.5926
    shape "0.5000"
  ]
  edge [
    source 1
    target 11
    key 0
    weight 14351.0
  ]
  edge [
    source 1
    target 78
    key 0
    weight 7854.0
  ]
  edge [
    source 1
    target 9
    key 0
    weight 13898.0
  ]
  edge [
    source 1
    target 8
    key 0
    weight 2037.0
  ]
  edge [
    source 1
    target 56
    key 0
    weight 13033.0
  ]
  edge [
    source 2
    target 11
    key 0
    weight 17197.0
  ]
  edge [
    source 2
    target 69
    key 0
    weight 1716.0
  ]
  edge [
    source 2
    target 34
    key 0
    weight 72387.0
  ]
  edge [
    source 2
    target 21
    key 0
    weight 10589.0
  ]
  edge [
    source 2
    target 50
    key 0
    weight 76989.0
  ]
  edge [
    source 2
    target 58
    key 0
    weight 9497.0
  ]
  edge [
    source 2
    target 65
    key 0
    weight 42536.0
  ]
  edge [
    source 2
    target 44
    key 0
    weight 24560.0
  ]
  edge [
    source 2
    target 33
    key 0
    weight 3384.0
  ]
  edge [
    source 2
    target 5
    key 0
    weight 304.0
  ]
  edge [
    source 3
    target 52
    key 0
    weight 15952.0
  ]
  edge [
    source 3
    target 23
    key 0
    weight 14600.0
  ]
  edge [
    source 3
    target 22
    key 0
    weight 13274.0
  ]
  edge [
    source 3
    target 76
    key 0
    weight 96667.0
  ]
  edge [
    source 3
    target 68
    key 0
    weight 21348.0
  ]
  edge [
    source 3
    target 71
    key 0
    weight 159097.0
  ]
  edge [
    source 3
    target 31
    key 0
    weight 90867.0
  ]
  edge [
    source 3
    target 70
    key 0
    weight 29524.0
  ]
  edge [
    source 3
    target 32
    key 0
    weight 806.0
  ]
  edge [
    source 3
    target 34
    key 0
    weight 19990.0
  ]
  edge [
    source 3
    target 17
    key 0
    weight 25688.0
  ]
  edge [
    source 3
    target 25
    key 0
    weight 996567.0
  ]
  edge [
    source 3
    target 49
    key 0
    weight 44835.0
  ]
  edge [
    source 3
    target 50
    key 0
    weight 3071.0
  ]
  edge [
    source 3
    target 6
    key 0
    weight 36169.0
  ]
  edge [
    source 3
    target 37
    key 0
    weight 115397.0
  ]
  edge [
    source 3
    target 2
    key 0
    weight 15533.0
  ]
  edge [
    source 3
    target 61
    key 0
    weight 154.0
  ]
  edge [
    source 3
    target 42
    key 0
    weight 253.0
  ]
  edge [
    source 3
    target 74
    key 0
    weight 1421.0
  ]
  edge [
    source 3
    target 75
    key 0
    weight 5957.0
  ]
  edge [
    source 3
    target 35
    key 0
    weight 8926.0
  ]
  edge [
    source 3
    target 19
    key 0
    weight 2916.0
  ]
  edge [
    source 3
    target 36
    key 0
    weight 10773.0
  ]
  edge [
    source 3
    target 60
    key 0
    weight 5449.0
  ]
  edge [
    source 3
    target 0
    key 0
    weight 3599.0
  ]
  edge [
    source 3
    target 66
    key 0
    weight 35286.0
  ]
  edge [
    source 3
    target 16
    key 0
    weight 48573.0
  ]
  edge [
    source 3
    target 26
    key 0
    weight 7555.0
  ]
  edge [
    source 3
    target 59
    key 0
    weight 15047.0
  ]
  edge [
    source 3
    target 63
    key 0
    weight 6253.0
  ]
  edge [
    source 3
    target 14
    key 0
    weight 9044.0
  ]
  edge [
    source 3
    target 67
    key 0
    weight 7741.0
  ]
  edge [
    source 4
    target 7
    key 0
    weight 502.0
  ]
  edge [
    source 4
    target 73
    key 0
    weight 4491.0
  ]
  edge [
    source 6
    target 27
    key 0
    weight 1586.0
  ]
  edge [
    source 6
    target 52
    key 0
    weight 21765.0
  ]
  edge [
    source 6
    target 23
    key 0
    weight 101487.0
  ]
  edge [
    source 6
    target 22
    key 0
    weight 9726.0
  ]
  edge [
    source 6
    target 45
    key 0
    weight 1811.0
  ]
  edge [
    source 6
    target 62
    key 0
    weight 2901.0
  ]
  edge [
    source 6
    target 76
    key 0
    weight 130180.0
  ]
  edge [
    source 6
    target 68
    key 0
    weight 42064.0
  ]
  edge [
    source 6
    target 69
    key 0
    weight 1500.0
  ]
  edge [
    source 6
    target 71
    key 0
    weight 31829.0
  ]
  edge [
    source 6
    target 31
    key 0
    weight 9062.0
  ]
  edge [
    source 6
    target 70
    key 0
    weight 23430.0
  ]
  edge [
    source 6
    target 17
    key 0
    weight 26894.0
  ]
  edge [
    source 6
    target 25
    key 0
    weight 398266.0
  ]
  edge [
    source 6
    target 49
    key 0
    weight 474054.0
  ]
  edge [
    source 6
    target 15
    key 0
    weight 1000.0
  ]
  edge [
    source 6
    target 50
    key 0
    weight 3881.0
  ]
  edge [
    source 6
    target 37
    key 0
    weight 79584.0
  ]
  edge [
    source 6
    target 74
    key 0
    weight 6130.0
  ]
  edge [
    source 6
    target 75
    key 0
    weight 12629.0
  ]
  edge [
    source 6
    target 65
    key 0
    weight 17130.0
  ]
  edge [
    source 6
    target 40
    key 0
    weight 26821.0
  ]
  edge [
    source 6
    target 43
    key 0
    weight 1518.0
  ]
  edge [
    source 6
    target 35
    key 0
    weight 9288.0
  ]
  edge [
    source 6
    target 46
    key 0
    weight 1594.0
  ]
  edge [
    source 6
    target 1
    key 0
    weight 10198.0
  ]
  edge [
    source 6
    target 19
    key 0
    weight 3662.0
  ]
  edge [
    source 6
    target 29
    key 0
    weight 2394.0
  ]
  edge [
    source 6
    target 36
    key 0
    weight 15385.0
  ]
  edge [
    source 6
    target 48
    key 0
    weight 4899.0
  ]
  edge [
    source 6
    target 9
    key 0
    weight 5043.0
  ]
  edge [
    source 6
    target 60
    key 0
    weight 13031.0
  ]
  edge [
    source 6
    target 0
    key 0
    weight 12994.0
  ]
  edge [
    source 6
    target 66
    key 0
    weight 8289.0
  ]
  edge [
    source 6
    target 79
    key 0
    weight 4057.0
  ]
  edge [
    source 6
    target 16
    key 0
    weight 6275.0
  ]
  edge [
    source 6
    target 26
    key 0
    weight 22260.0
  ]
  edge [
    source 6
    target 33
    key 0
    weight 8709.0
  ]
  edge [
    source 6
    target 59
    key 0
    weight 15503.0
  ]
  edge [
    source 6
    target 63
    key 0
    weight 1651.0
  ]
  edge [
    source 6
    target 39
    key 0
    weight 1170.0
  ]
  edge [
    source 6
    target 3
    key 0
    weight 32959.0
  ]
  edge [
    source 6
    target 14
    key 0
    weight 1433.0
  ]
  edge [
    source 6
    target 67
    key 0
    weight 21759.0
  ]
  edge [
    source 6
    target 18
    key 0
    weight 2158.0
  ]
  edge [
    source 8
    target 4
    key 0
    weight 517.0
  ]
  edge [
    source 9
    target 11
    key 0
    weight 61905.0
  ]
  edge [
    source 9
    target 21
    key 0
    weight 358.0
  ]
  edge [
    source 9
    target 77
    key 0
    weight 140405.0
  ]
  edge [
    source 9
    target 47
    key 0
    weight 48218.0
  ]
  edge [
    source 9
    target 7
    key 0
    weight 193.0
  ]
  edge [
    source 9
    target 1
    key 0
    weight 172885.0
  ]
  edge [
    source 9
    target 78
    key 0
    weight 26605.0
  ]
  edge [
    source 9
    target 13
    key 0
    weight 13297.0
  ]
  edge [
    source 9
    target 28
    key 0
    weight 2646.0
  ]
  edge [
    source 9
    target 29
    key 0
    weight 1234.0
  ]
  edge [
    source 9
    target 20
    key 0
    weight 1998.0
  ]
  edge [
    source 9
    target 8
    key 0
    weight 15609.0
  ]
  edge [
    source 9
    target 73
    key 0
    weight 936.0
  ]
  edge [
    source 9
    target 55
    key 0
    weight 3415.0
  ]
  edge [
    source 9
    target 56
    key 0
    weight 30576.0
  ]
  edge [
    source 9
    target 79
    key 0
    weight 8052.0
  ]
  edge [
    source 9
    target 57
    key 0
    weight 15743.0
  ]
  edge [
    source 9
    target 4
    key 0
    weight 231.0
  ]
  edge [
    source 9
    target 67
    key 0
    weight 7416.0
  ]
  edge [
    source 9
    target 18
    key 0
    weight 8281.0
  ]
  edge [
    source 10
    target 11
    key 0
    weight 5490.0
  ]
  edge [
    source 10
    target 77
    key 0
    weight 2032121.0
  ]
  edge [
    source 10
    target 50
    key 0
    weight 4312.0
  ]
  edge [
    source 10
    target 2
    key 0
    weight 13925.0
  ]
  edge [
    source 10
    target 47
    key 0
    weight 35460.0
  ]
  edge [
    source 10
    target 7
    key 0
    weight 210.0
  ]
  edge [
    source 10
    target 40
    key 0
    weight 18832.0
  ]
  edge [
    source 10
    target 51
    key 0
    weight 334.0
  ]
  edge [
    source 10
    target 13
    key 0
    weight 3704.0
  ]
  edge [
    source 10
    target 73
    key 0
    weight 1126.0
  ]
  edge [
    source 10
    target 55
    key 0
    weight 948.0
  ]
  edge [
    source 10
    target 79
    key 0
    weight 2133.0
  ]
  edge [
    source 10
    target 57
    key 0
    weight 1788.0
  ]
  edge [
    source 10
    target 39
    key 0
    weight 2684.0
  ]
  edge [
    source 10
    target 4
    key 0
    weight 614.0
  ]
  edge [
    source 10
    target 18
    key 0
    weight 1350.0
  ]
  edge [
    source 11
    target 1
    key 0
    weight 19116.0
  ]
  edge [
    source 11
    target 78
    key 0
    weight 1482.0
  ]
  edge [
    source 11
    target 8
    key 0
    weight 4114.0
  ]
  edge [
    source 11
    target 56
    key 0
    weight 890.0
  ]
  edge [
    source 11
    target 57
    key 0
    weight 11265.0
  ]
  edge [
    source 12
    target 52
    key 0
    weight 13802.0
  ]
  edge [
    source 12
    target 11
    key 0
    weight 12624.0
  ]
  edge [
    source 12
    target 30
    key 0
    weight 1210975.0
  ]
  edge [
    source 12
    target 22
    key 0
    weight 12848.0
  ]
  edge [
    source 12
    target 62
    key 0
    weight 869.0
  ]
  edge [
    source 12
    target 76
    key 0
    weight 114947.0
  ]
  edge [
    source 12
    target 68
    key 0
    weight 59911.0
  ]
  edge [
    source 12
    target 69
    key 0
    weight 7923.0
  ]
  edge [
    source 12
    target 70
    key 0
    weight 23543.0
  ]
  edge [
    source 12
    target 10
    key 0
    weight 107506.0
  ]
  edge [
    source 12
    target 34
    key 0
    weight 32817.0
  ]
  edge [
    source 12
    target 54
    key 0
    weight 13953.0
  ]
  edge [
    source 12
    target 17
    key 0
    weight 28148.0
  ]
  edge [
    source 12
    target 21
    key 0
    weight 1813.0
  ]
  edge [
    source 12
    target 77
    key 0
    weight 1066964.0
  ]
  edge [
    source 12
    target 25
    key 0
    weight 405577.0
  ]
  edge [
    source 12
    target 49
    key 0
    weight 77687.0
  ]
  edge [
    source 12
    target 15
    key 0
    weight 2418.0
  ]
  edge [
    source 12
    target 50
    key 0
    weight 12810.0
  ]
  edge [
    source 12
    target 6
    key 0
    weight 30181.0
  ]
  edge [
    source 12
    target 38
    key 0
    weight 290585.0
  ]
  edge [
    source 12
    target 37
    key 0
    weight 69696.0
  ]
  edge [
    source 12
    target 2
    key 0
    weight 90620.0
  ]
  edge [
    source 12
    target 58
    key 0
    weight 19865.0
  ]
  edge [
    source 12
    target 74
    key 0
    weight 2539.0
  ]
  edge [
    source 12
    target 75
    key 0
    weight 4800.0
  ]
  edge [
    source 12
    target 7
    key 0
    weight 493.0
  ]
  edge [
    source 12
    target 65
    key 0
    weight 113147.0
  ]
  edge [
    source 12
    target 40
    key 0
    weight 53582.0
  ]
  edge [
    source 12
    target 43
    key 0
    weight 1843.0
  ]
  edge [
    source 12
    target 46
    key 0
    weight 3506.0
  ]
  edge [
    source 12
    target 1
    key 0
    weight 9033.0
  ]
  edge [
    source 12
    target 78
    key 0
    weight 2057.0
  ]
  edge [
    source 12
    target 13
    key 0
    weight 2537.0
  ]
  edge [
    source 12
    target 19
    key 0
    weight 14342.0
  ]
  edge [
    source 12
    target 28
    key 0
    weight 1398.0
  ]
  edge [
    source 12
    target 29
    key 0
    weight 1180.0
  ]
  edge [
    source 12
    target 48
    key 0
    weight 7077.0
  ]
  edge [
    source 12
    target 20
    key 0
    weight 3066.0
  ]
  edge [
    source 12
    target 8
    key 0
    weight 1296.0
  ]
  edge [
    source 12
    target 44
    key 0
    weight 39617.0
  ]
  edge [
    source 12
    target 73
    key 0
    weight 1494.0
  ]
  edge [
    source 12
    target 60
    key 0
    weight 11485.0
  ]
  edge [
    source 12
    target 0
    key 0
    weight 11709.0
  ]
  edge [
    source 12
    target 26
    key 0
    weight 21471.0
  ]
  edge [
    source 12
    target 33
    key 0
    weight 3515.0
  ]
  edge [
    source 12
    target 41
    key 0
    weight 9232.0
  ]
  edge [
    source 12
    target 59
    key 0
    weight 7180.0
  ]
  edge [
    source 12
    target 57
    key 0
    weight 6238.0
  ]
  edge [
    source 12
    target 72
    key 0
    weight 44387.0
  ]
  edge [
    source 12
    target 39
    key 0
    weight 8014.0
  ]
  edge [
    source 12
    target 64
    key 0
    weight 167.0
  ]
  edge [
    source 12
    target 5
    key 0
    weight 5217.0
  ]
  edge [
    source 12
    target 14
    key 0
    weight 2420.0
  ]
  edge [
    source 12
    target 4
    key 0
    weight 312.0
  ]
  edge [
    source 12
    target 67
    key 0
    weight 23166.0
  ]
  edge [
    source 12
    target 18
    key 0
    weight 3203.0
  ]
  edge [
    source 13
    target 11
    key 0
    weight 4574.0
  ]
  edge [
    source 13
    target 51
    key 0
    weight 482.0
  ]
  edge [
    source 13
    target 28
    key 0
    weight 3643.0
  ]
  edge [
    source 13
    target 29
    key 0
    weight 1356.0
  ]
  edge [
    source 13
    target 20
    key 0
    weight 1735.0
  ]
  edge [
    source 13
    target 8
    key 0
    weight 1596.0
  ]
  edge [
    source 13
    target 55
    key 0
    weight 1812.0
  ]
  edge [
    source 13
    target 79
    key 0
    weight 9711.0
  ]
  edge [
    source 13
    target 57
    key 0
    weight 8267.0
  ]
  edge [
    source 13
    target 18
    key 0
    weight 13093.0
  ]
  edge [
    source 14
    target 66
    key 0
    weight 11930.0
  ]
  edge [
    source 16
    target 31
    key 0
    weight 13004.0
  ]
  edge [
    source 16
    target 25
    key 0
    weight 368238.0
  ]
  edge [
    source 16
    target 19
    key 0
    weight 7985.0
  ]
  edge [
    source 16
    target 66
    key 0
    weight 4357.0
  ]
  edge [
    source 16
    target 59
    key 0
    weight 8238.0
  ]
  edge [
    source 16
    target 63
    key 0
    weight 1566.0
  ]
  edge [
    source 16
    target 3
    key 0
    weight 40019.0
  ]
  edge [
    source 16
    target 14
    key 0
    weight 2891.0
  ]
  edge [
    source 17
    target 52
    key 0
    weight 81733.0
  ]
  edge [
    source 17
    target 23
    key 0
    weight 16572.0
  ]
  edge [
    source 17
    target 22
    key 0
    weight 16806.0
  ]
  edge [
    source 17
    target 76
    key 0
    weight 87959.0
  ]
  edge [
    source 17
    target 71
    key 0
    weight 34693.0
  ]
  edge [
    source 17
    target 31
    key 0
    weight 5297.0
  ]
  edge [
    source 17
    target 70
    key 0
    weight 121647.0
  ]
  edge [
    source 17
    target 32
    key 0
    weight 8844.0
  ]
  edge [
    source 17
    target 25
    key 0
    weight 218619.0
  ]
  edge [
    source 17
    target 49
    key 0
    weight 76037.0
  ]
  edge [
    source 17
    target 15
    key 0
    weight 1284.0
  ]
  edge [
    source 17
    target 6
    key 0
    weight 30876.0
  ]
  edge [
    source 17
    target 42
    key 0
    weight 338.0
  ]
  edge [
    source 17
    target 59
    key 0
    weight 6576.0
  ]
  edge [
    source 17
    target 3
    key 0
    weight 20259.0
  ]
  edge [
    source 17
    target 4
    key 0
    weight 221.0
  ]
  edge [
    source 18
    target 13
    key 0
    weight 5251.0
  ]
  edge [
    source 18
    target 57
    key 0
    weight 6281.0
  ]
  edge [
    source 19
    target 74
    key 0
    weight 2153.0
  ]
  edge [
    source 19
    target 39
    key 0
    weight 1873.0
  ]
  edge [
    source 20
    target 51
    key 0
    weight 2529.0
  ]
  edge [
    source 20
    target 28
    key 0
    weight 7111.0
  ]
  edge [
    source 20
    target 29
    key 0
    weight 793.0
  ]
  edge [
    source 20
    target 55
    key 0
    weight 951.0
  ]
  edge [
    source 22
    target 52
    key 0
    weight 57521.0
  ]
  edge [
    source 22
    target 70
    key 0
    weight 108793.0
  ]
  edge [
    source 22
    target 32
    key 0
    weight 814.0
  ]
  edge [
    source 22
    target 17
    key 0
    weight 12295.0
  ]
  edge [
    source 22
    target 42
    key 0
    weight 3531.0
  ]
  edge [
    source 22
    target 59
    key 0
    weight 7793.0
  ]
  edge [
    source 22
    target 63
    key 0
    weight 3593.0
  ]
  edge [
    source 23
    target 24
    key 0
    weight 23023.0
  ]
  edge [
    source 23
    target 27
    key 0
    weight 47886.0
  ]
  edge [
    source 23
    target 52
    key 0
    weight 70505.0
  ]
  edge [
    source 23
    target 11
    key 0
    weight 3861.0
  ]
  edge [
    source 23
    target 22
    key 0
    weight 19778.0
  ]
  edge [
    source 23
    target 45
    key 0
    weight 52146.0
  ]
  edge [
    source 23
    target 62
    key 0
    weight 67464.0
  ]
  edge [
    source 23
    target 76
    key 0
    weight 306259.0
  ]
  edge [
    source 23
    target 68
    key 0
    weight 354356.0
  ]
  edge [
    source 23
    target 69
    key 0
    weight 974.0
  ]
  edge [
    source 23
    target 71
    key 0
    weight 184576.0
  ]
  edge [
    source 23
    target 31
    key 0
    weight 10031.0
  ]
  edge [
    source 23
    target 70
    key 0
    weight 68207.0
  ]
  edge [
    source 23
    target 10
    key 0
    weight 50382.0
  ]
  edge [
    source 23
    target 32
    key 0
    weight 1666.0
  ]
  edge [
    source 23
    target 54
    key 0
    weight 3623.0
  ]
  edge [
    source 23
    target 17
    key 0
    weight 38405.0
  ]
  edge [
    source 23
    target 77
    key 0
    weight 328783.0
  ]
  edge [
    source 23
    target 25
    key 0
    weight 742989.0
  ]
  edge [
    source 23
    target 49
    key 0
    weight 160645.0
  ]
  edge [
    source 23
    target 15
    key 0
    weight 3445.0
  ]
  edge [
    source 23
    target 6
    key 0
    weight 523392.0
  ]
  edge [
    source 23
    target 38
    key 0
    weight 64256.0
  ]
  edge [
    source 23
    target 37
    key 0
    weight 289608.0
  ]
  edge [
    source 23
    target 2
    key 0
    weight 20979.0
  ]
  edge [
    source 23
    target 47
    key 0
    weight 42555.0
  ]
  edge [
    source 23
    target 42
    key 0
    weight 935.0
  ]
  edge [
    source 23
    target 74
    key 0
    weight 41597.0
  ]
  edge [
    source 23
    target 75
    key 0
    weight 58815.0
  ]
  edge [
    source 23
    target 65
    key 0
    weight 20354.0
  ]
  edge [
    source 23
    target 40
    key 0
    weight 24057.0
  ]
  edge [
    source 23
    target 43
    key 0
    weight 7753.0
  ]
  edge [
    source 23
    target 35
    key 0
    weight 16335.0
  ]
  edge [
    source 23
    target 46
    key 0
    weight 6364.0
  ]
  edge [
    source 23
    target 1
    key 0
    weight 29305.0
  ]
  edge [
    source 23
    target 53
    key 0
    weight 1998.0
  ]
  edge [
    source 23
    target 78
    key 0
    weight 1442.0
  ]
  edge [
    source 23
    target 13
    key 0
    weight 4080.0
  ]
  edge [
    source 23
    target 19
    key 0
    weight 9579.0
  ]
  edge [
    source 23
    target 36
    key 0
    weight 27960.0
  ]
  edge [
    source 23
    target 48
    key 0
    weight 40391.0
  ]
  edge [
    source 23
    target 9
    key 0
    weight 28613.0
  ]
  edge [
    source 23
    target 60
    key 0
    weight 73710.0
  ]
  edge [
    source 23
    target 0
    key 0
    weight 82104.0
  ]
  edge [
    source 23
    target 66
    key 0
    weight 23704.0
  ]
  edge [
    source 23
    target 79
    key 0
    weight 2197.0
  ]
  edge [
    source 23
    target 16
    key 0
    weight 12434.0
  ]
  edge [
    source 23
    target 26
    key 0
    weight 34415.0
  ]
  edge [
    source 23
    target 33
    key 0
    weight 13223.0
  ]
  edge [
    source 23
    target 41
    key 0
    weight 7155.0
  ]
  edge [
    source 23
    target 59
    key 0
    weight 13104.0
  ]
  edge [
    source 23
    target 63
    key 0
    weight 6886.0
  ]
  edge [
    source 23
    target 39
    key 0
    weight 2160.0
  ]
  edge [
    source 23
    target 3
    key 0
    weight 45643.0
  ]
  edge [
    source 23
    target 64
    key 0
    weight 449.0
  ]
  edge [
    source 23
    target 67
    key 0
    weight 21617.0
  ]
  edge [
    source 25
    target 24
    key 0
    weight 12714.0
  ]
  edge [
    source 25
    target 27
    key 0
    weight 1242.0
  ]
  edge [
    source 25
    target 52
    key 0
    weight 135528.0
  ]
  edge [
    source 25
    target 23
    key 0
    weight 222186.0
  ]
  edge [
    source 25
    target 11
    key 0
    weight 17066.0
  ]
  edge [
    source 25
    target 30
    key 0
    weight 76823.0
  ]
  edge [
    source 25
    target 22
    key 0
    weight 141617.0
  ]
  edge [
    source 25
    target 45
    key 0
    weight 855.0
  ]
  edge [
    source 25
    target 62
    key 0
    weight 1188.0
  ]
  edge [
    source 25
    target 76
    key 0
    weight 882784.0
  ]
  edge [
    source 25
    target 68
    key 0
    weight 475694.0
  ]
  edge [
    source 25
    target 12
    key 0
    weight 57365.0
  ]
  edge [
    source 25
    target 69
    key 0
    weight 4250.0
  ]
  edge [
    source 25
    target 71
    key 0
    weight 970830.0
  ]
  edge [
    source 25
    target 31
    key 0
    weight 129067.0
  ]
  edge [
    source 25
    target 70
    key 0
    weight 437006.0
  ]
  edge [
    source 25
    target 10
    key 0
    weight 93533.0
  ]
  edge [
    source 25
    target 32
    key 0
    weight 11662.0
  ]
  edge [
    source 25
    target 34
    key 0
    weight 53149.0
  ]
  edge [
    source 25
    target 54
    key 0
    weight 4717.0
  ]
  edge [
    source 25
    target 17
    key 0
    weight 358165.0
  ]
  edge [
    source 25
    target 77
    key 0
    weight 781766.0
  ]
  edge [
    source 25
    target 49
    key 0
    weight 1148108.0
  ]
  edge [
    source 25
    target 15
    key 0
    weight 7835.0
  ]
  edge [
    source 25
    target 50
    key 0
    weight 14160.0
  ]
  edge [
    source 25
    target 6
    key 0
    weight 798074.0
  ]
  edge [
    source 25
    target 38
    key 0
    weight 156272.0
  ]
  edge [
    source 25
    target 37
    key 0
    weight 689710.0
  ]
  edge [
    source 25
    target 2
    key 0
    weight 99820.0
  ]
  edge [
    source 25
    target 61
    key 0
    weight 221.0
  ]
  edge [
    source 25
    target 47
    key 0
    weight 83411.0
  ]
  edge [
    source 25
    target 58
    key 0
    weight 5947.0
  ]
  edge [
    source 25
    target 42
    key 0
    weight 5835.0
  ]
  edge [
    source 25
    target 74
    key 0
    weight 15582.0
  ]
  edge [
    source 25
    target 75
    key 0
    weight 90816.0
  ]
  edge [
    source 25
    target 65
    key 0
    weight 113459.0
  ]
  edge [
    source 25
    target 40
    key 0
    weight 147775.0
  ]
  edge [
    source 25
    target 43
    key 0
    weight 816.0
  ]
  edge [
    source 25
    target 35
    key 0
    weight 55202.0
  ]
  edge [
    source 25
    target 46
    key 0
    weight 1750.0
  ]
  edge [
    source 25
    target 51
    key 0
    weight 549.0
  ]
  edge [
    source 25
    target 1
    key 0
    weight 29333.0
  ]
  edge [
    source 25
    target 53
    key 0
    weight 3788.0
  ]
  edge [
    source 25
    target 78
    key 0
    weight 1855.0
  ]
  edge [
    source 25
    target 13
    key 0
    weight 12449.0
  ]
  edge [
    source 25
    target 19
    key 0
    weight 25891.0
  ]
  edge [
    source 25
    target 28
    key 0
    weight 3791.0
  ]
  edge [
    source 25
    target 29
    key 0
    weight 1507.0
  ]
  edge [
    source 25
    target 36
    key 0
    weight 68854.0
  ]
  edge [
    source 25
    target 48
    key 0
    weight 7563.0
  ]
  edge [
    source 25
    target 9
    key 0
    weight 76524.0
  ]
  edge [
    source 25
    target 20
    key 0
    weight 2675.0
  ]
  edge [
    source 25
    target 8
    key 0
    weight 1711.0
  ]
  edge [
    source 25
    target 44
    key 0
    weight 47215.0
  ]
  edge [
    source 25
    target 73
    key 0
    weight 835.0
  ]
  edge [
    source 25
    target 55
    key 0
    weight 983.0
  ]
  edge [
    source 25
    target 56
    key 0
    weight 2158.0
  ]
  edge [
    source 25
    target 60
    key 0
    weight 75301.0
  ]
  edge [
    source 25
    target 0
    key 0
    weight 23436.0
  ]
  edge [
    source 25
    target 66
    key 0
    weight 68588.0
  ]
  edge [
    source 25
    target 79
    key 0
    weight 13060.0
  ]
  edge [
    source 25
    target 16
    key 0
    weight 158277.0
  ]
  edge [
    source 25
    target 26
    key 0
    weight 85851.0
  ]
  edge [
    source 25
    target 33
    key 0
    weight 39164.0
  ]
  edge [
    source 25
    target 41
    key 0
    weight 22008.0
  ]
  edge [
    source 25
    target 59
    key 0
    weight 239865.0
  ]
  edge [
    source 25
    target 63
    key 0
    weight 40489.0
  ]
  edge [
    source 25
    target 57
    key 0
    weight 6065.0
  ]
  edge [
    source 25
    target 72
    key 0
    weight 58662.0
  ]
  edge [
    source 25
    target 39
    key 0
    weight 5695.0
  ]
  edge [
    source 25
    target 3
    key 0
    weight 1143072.0
  ]
  edge [
    source 25
    target 64
    key 0
    weight 2812.0
  ]
  edge [
    source 25
    target 5
    key 0
    weight 2205.0
  ]
  edge [
    source 25
    target 14
    key 0
    weight 19657.0
  ]
  edge [
    source 25
    target 4
    key 0
    weight 341.0
  ]
  edge [
    source 25
    target 67
    key 0
    weight 99410.0
  ]
  edge [
    source 25
    target 18
    key 0
    weight 3495.0
  ]
  edge [
    source 26
    target 15
    key 0
    weight 13534.0
  ]
  edge [
    source 26
    target 29
    key 0
    weight 424.0
  ]
  edge [
    source 26
    target 63
    key 0
    weight 1677.0
  ]
  edge [
    source 26
    target 39
    key 0
    weight 849.0
  ]
  edge [
    source 28
    target 7
    key 0
    weight 399.0
  ]
  edge [
    source 28
    target 51
    key 0
    weight 1401.0
  ]
  edge [
    source 28
    target 29
    key 0
    weight 2804.0
  ]
  edge [
    source 28
    target 20
    key 0
    weight 11049.0
  ]
  edge [
    source 29
    target 51
    key 0
    weight 1032.0
  ]
  edge [
    source 29
    target 20
    key 0
    weight 2934.0
  ]
  edge [
    source 30
    target 76
    key 0
    weight 108585.0
  ]
  edge [
    source 30
    target 12
    key 0
    weight 271849.0
  ]
  edge [
    source 30
    target 69
    key 0
    weight 3703.0
  ]
  edge [
    source 30
    target 34
    key 0
    weight 12604.0
  ]
  edge [
    source 30
    target 21
    key 0
    weight 677.0
  ]
  edge [
    source 30
    target 77
    key 0
    weight 135643.0
  ]
  edge [
    source 30
    target 49
    key 0
    weight 33281.0
  ]
  edge [
    source 30
    target 50
    key 0
    weight 4059.0
  ]
  edge [
    source 30
    target 38
    key 0
    weight 45445.0
  ]
  edge [
    source 30
    target 2
    key 0
    weight 13644.0
  ]
  edge [
    source 30
    target 58
    key 0
    weight 23396.0
  ]
  edge [
    source 30
    target 65
    key 0
    weight 55398.0
  ]
  edge [
    source 30
    target 46
    key 0
    weight 1356.0
  ]
  edge [
    source 30
    target 1
    key 0
    weight 12574.0
  ]
  edge [
    source 30
    target 78
    key 0
    weight 1971.0
  ]
  edge [
    source 30
    target 19
    key 0
    weight 2695.0
  ]
  edge [
    source 30
    target 28
    key 0
    weight 1929.0
  ]
  edge [
    source 30
    target 29
    key 0
    weight 485.0
  ]
  edge [
    source 30
    target 36
    key 0
    weight 9218.0
  ]
  edge [
    source 30
    target 9
    key 0
    weight 7147.0
  ]
  edge [
    source 30
    target 20
    key 0
    weight 1112.0
  ]
  edge [
    source 30
    target 44
    key 0
    weight 24159.0
  ]
  edge [
    source 30
    target 55
    key 0
    weight 1346.0
  ]
  edge [
    source 30
    target 56
    key 0
    weight 2655.0
  ]
  edge [
    source 30
    target 79
    key 0
    weight 2767.0
  ]
  edge [
    source 30
    target 72
    key 0
    weight 23642.0
  ]
  edge [
    source 30
    target 64
    key 0
    weight 274.0
  ]
  edge [
    source 30
    target 5
    key 0
    weight 1050.0
  ]
  edge [
    source 30
    target 67
    key 0
    weight 21277.0
  ]
  edge [
    source 30
    target 18
    key 0
    weight 1195.0
  ]
  edge [
    source 31
    target 25
    key 0
    weight 158903.0
  ]
  edge [
    source 31
    target 61
    key 0
    weight 194.0
  ]
  edge [
    source 31
    target 63
    key 0
    weight 3913.0
  ]
  edge [
    source 31
    target 3
    key 0
    weight 31549.0
  ]
  edge [
    source 31
    target 14
    key 0
    weight 1865.0
  ]
  edge [
    source 33
    target 76
    key 0
    weight 100138.0
  ]
  edge [
    source 33
    target 69
    key 0
    weight 7451.0
  ]
  edge [
    source 33
    target 21
    key 0
    weight 484.0
  ]
  edge [
    source 33
    target 77
    key 0
    weight 240032.0
  ]
  edge [
    source 33
    target 50
    key 0
    weight 2923.0
  ]
  edge [
    source 33
    target 2
    key 0
    weight 16982.0
  ]
  edge [
    source 33
    target 58
    key 0
    weight 7885.0
  ]
  edge [
    source 33
    target 65
    key 0
    weight 23269.0
  ]
  edge [
    source 33
    target 43
    key 0
    weight 343.0
  ]
  edge [
    source 33
    target 46
    key 0
    weight 3288.0
  ]
  edge [
    source 33
    target 53
    key 0
    weight 4151.0
  ]
  edge [
    source 33
    target 19
    key 0
    weight 4306.0
  ]
  edge [
    source 33
    target 44
    key 0
    weight 15817.0
  ]
  edge [
    source 33
    target 73
    key 0
    weight 622.0
  ]
  edge [
    source 33
    target 0
    key 0
    weight 4890.0
  ]
  edge [
    source 33
    target 41
    key 0
    weight 6408.0
  ]
  edge [
    source 33
    target 39
    key 0
    weight 2606.0
  ]
  edge [
    source 33
    target 64
    key 0
    weight 207.0
  ]
  edge [
    source 33
    target 5
    key 0
    weight 1089.0
  ]
  edge [
    source 33
    target 67
    key 0
    weight 9483.0
  ]
  edge [
    source 34
    target 69
    key 0
    weight 1011.0
  ]
  edge [
    source 34
    target 38
    key 0
    weight 65894.0
  ]
  edge [
    source 34
    target 58
    key 0
    weight 6469.0
  ]
  edge [
    source 34
    target 44
    key 0
    weight 32298.0
  ]
  edge [
    source 35
    target 76
    key 0
    weight 152674.0
  ]
  edge [
    source 36
    target 63
    key 0
    weight 1524.0
  ]
  edge [
    source 36
    target 67
    key 0
    weight 7621.0
  ]
  edge [
    source 37
    target 24
    key 0
    weight 1243.0
  ]
  edge [
    source 37
    target 27
    key 0
    weight 6045.0
  ]
  edge [
    source 37
    target 52
    key 0
    weight 50337.0
  ]
  edge [
    source 37
    target 23
    key 0
    weight 74172.0
  ]
  edge [
    source 37
    target 11
    key 0
    weight 15325.0
  ]
  edge [
    source 37
    target 30
    key 0
    weight 41006.0
  ]
  edge [
    source 37
    target 22
    key 0
    weight 40169.0
  ]
  edge [
    source 37
    target 45
    key 0
    weight 4083.0
  ]
  edge [
    source 37
    target 62
    key 0
    weight 6776.0
  ]
  edge [
    source 37
    target 76
    key 0
    weight 322587.0
  ]
  edge [
    source 37
    target 68
    key 0
    weight 401791.0
  ]
  edge [
    source 37
    target 12
    key 0
    weight 54888.0
  ]
  edge [
    source 37
    target 69
    key 0
    weight 1398.0
  ]
  edge [
    source 37
    target 71
    key 0
    weight 289190.0
  ]
  edge [
    source 37
    target 31
    key 0
    weight 58484.0
  ]
  edge [
    source 37
    target 70
    key 0
    weight 79917.0
  ]
  edge [
    source 37
    target 10
    key 0
    weight 56025.0
  ]
  edge [
    source 37
    target 32
    key 0
    weight 2058.0
  ]
  edge [
    source 37
    target 34
    key 0
    weight 51412.0
  ]
  edge [
    source 37
    target 54
    key 0
    weight 5189.0
  ]
  edge [
    source 37
    target 17
    key 0
    weight 50685.0
  ]
  edge [
    source 37
    target 77
    key 0
    weight 305554.0
  ]
  edge [
    source 37
    target 25
    key 0
    weight 1019389.0
  ]
  edge [
    source 37
    target 49
    key 0
    weight 144785.0
  ]
  edge [
    source 37
    target 15
    key 0
    weight 21483.0
  ]
  edge [
    source 37
    target 50
    key 0
    weight 9437.0
  ]
  edge [
    source 37
    target 6
    key 0
    weight 192407.0
  ]
  edge [
    source 37
    target 38
    key 0
    weight 43990.0
  ]
  edge [
    source 37
    target 2
    key 0
    weight 46977.0
  ]
  edge [
    source 37
    target 61
    key 0
    weight 144.0
  ]
  edge [
    source 37
    target 42
    key 0
    weight 629.0
  ]
  edge [
    source 37
    target 74
    key 0
    weight 30694.0
  ]
  edge [
    source 37
    target 75
    key 0
    weight 70624.0
  ]
  edge [
    source 37
    target 65
    key 0
    weight 70177.0
  ]
  edge [
    source 37
    target 40
    key 0
    weight 33031.0
  ]
  edge [
    source 37
    target 43
    key 0
    weight 832.0
  ]
  edge [
    source 37
    target 35
    key 0
    weight 13825.0
  ]
  edge [
    source 37
    target 46
    key 0
    weight 1670.0
  ]
  edge [
    source 37
    target 1
    key 0
    weight 38225.0
  ]
  edge [
    source 37
    target 53
    key 0
    weight 3791.0
  ]
  edge [
    source 37
    target 78
    key 0
    weight 4701.0
  ]
  edge [
    source 37
    target 13
    key 0
    weight 7573.0
  ]
  edge [
    source 37
    target 19
    key 0
    weight 26232.0
  ]
  edge [
    source 37
    target 28
    key 0
    weight 1574.0
  ]
  edge [
    source 37
    target 36
    key 0
    weight 106401.0
  ]
  edge [
    source 37
    target 48
    key 0
    weight 18615.0
  ]
  edge [
    source 37
    target 9
    key 0
    weight 31563.0
  ]
  edge [
    source 37
    target 20
    key 0
    weight 1696.0
  ]
  edge [
    source 37
    target 8
    key 0
    weight 843.0
  ]
  edge [
    source 37
    target 44
    key 0
    weight 20465.0
  ]
  edge [
    source 37
    target 73
    key 0
    weight 1159.0
  ]
  edge [
    source 37
    target 55
    key 0
    weight 2560.0
  ]
  edge [
    source 37
    target 56
    key 0
    weight 1331.0
  ]
  edge [
    source 37
    target 60
    key 0
    weight 103927.0
  ]
  edge [
    source 37
    target 0
    key 0
    weight 48478.0
  ]
  edge [
    source 37
    target 66
    key 0
    weight 51024.0
  ]
  edge [
    source 37
    target 79
    key 0
    weight 13418.0
  ]
  edge [
    source 37
    target 16
    key 0
    weight 38564.0
  ]
  edge [
    source 37
    target 26
    key 0
    weight 138424.0
  ]
  edge [
    source 37
    target 33
    key 0
    weight 16450.0
  ]
  edge [
    source 37
    target 41
    key 0
    weight 34341.0
  ]
  edge [
    source 37
    target 59
    key 0
    weight 85965.0
  ]
  edge [
    source 37
    target 63
    key 0
    weight 19454.0
  ]
  edge [
    source 37
    target 57
    key 0
    weight 3885.0
  ]
  edge [
    source 37
    target 72
    key 0
    weight 27963.0
  ]
  edge [
    source 37
    target 39
    key 0
    weight 19263.0
  ]
  edge [
    source 37
    target 3
    key 0
    weight 216031.0
  ]
  edge [
    source 37
    target 64
    key 0
    weight 441.0
  ]
  edge [
    source 37
    target 14
    key 0
    weight 22229.0
  ]
  edge [
    source 37
    target 4
    key 0
    weight 290.0
  ]
  edge [
    source 37
    target 67
    key 0
    weight 31140.0
  ]
  edge [
    source 37
    target 18
    key 0
    weight 3277.0
  ]
  edge [
    source 38
    target 52
    key 0
    weight 14864.0
  ]
  edge [
    source 38
    target 11
    key 0
    weight 6195.0
  ]
  edge [
    source 38
    target 30
    key 0
    weight 306474.0
  ]
  edge [
    source 38
    target 22
    key 0
    weight 11286.0
  ]
  edge [
    source 38
    target 76
    key 0
    weight 220662.0
  ]
  edge [
    source 38
    target 68
    key 0
    weight 60330.0
  ]
  edge [
    source 38
    target 12
    key 0
    weight 339757.0
  ]
  edge [
    source 38
    target 69
    key 0
    weight 12016.0
  ]
  edge [
    source 38
    target 71
    key 0
    weight 30748.0
  ]
  edge [
    source 38
    target 31
    key 0
    weight 8010.0
  ]
  edge [
    source 38
    target 70
    key 0
    weight 102912.0
  ]
  edge [
    source 38
    target 10
    key 0
    weight 149047.0
  ]
  edge [
    source 38
    target 34
    key 0
    weight 134557.0
  ]
  edge [
    source 38
    target 54
    key 0
    weight 10388.0
  ]
  edge [
    source 38
    target 17
    key 0
    weight 11399.0
  ]
  edge [
    source 38
    target 21
    key 0
    weight 644.0
  ]
  edge [
    source 38
    target 77
    key 0
    weight 1982651.0
  ]
  edge [
    source 38
    target 25
    key 0
    weight 209465.0
  ]
  edge [
    source 38
    target 49
    key 0
    weight 98557.0
  ]
  edge [
    source 38
    target 15
    key 0
    weight 1717.0
  ]
  edge [
    source 38
    target 50
    key 0
    weight 15470.0
  ]
  edge [
    source 38
    target 6
    key 0
    weight 46948.0
  ]
  edge [
    source 38
    target 37
    key 0
    weight 69372.0
  ]
  edge [
    source 38
    target 2
    key 0
    weight 115283.0
  ]
  edge [
    source 38
    target 47
    key 0
    weight 161778.0
  ]
  edge [
    source 38
    target 58
    key 0
    weight 138348.0
  ]
  edge [
    source 38
    target 74
    key 0
    weight 1765.0
  ]
  edge [
    source 38
    target 75
    key 0
    weight 29234.0
  ]
  edge [
    source 38
    target 65
    key 0
    weight 349443.0
  ]
  edge [
    source 38
    target 40
    key 0
    weight 489714.0
  ]
  edge [
    source 38
    target 43
    key 0
    weight 2042.0
  ]
  edge [
    source 38
    target 35
    key 0
    weight 11597.0
  ]
  edge [
    source 38
    target 46
    key 0
    weight 1278.0
  ]
  edge [
    source 38
    target 51
    key 0
    weight 1399.0
  ]
  edge [
    source 38
    target 53
    key 0
    weight 1594.0
  ]
  edge [
    source 38
    target 13
    key 0
    weight 6483.0
  ]
  edge [
    source 38
    target 19
    key 0
    weight 7454.0
  ]
  edge [
    source 38
    target 28
    key 0
    weight 1012.0
  ]
  edge [
    source 38
    target 36
    key 0
    weight 7488.0
  ]
  edge [
    source 38
    target 48
    key 0
    weight 1582.0
  ]
  edge [
    source 38
    target 9
    key 0
    weight 23335.0
  ]
  edge [
    source 38
    target 20
    key 0
    weight 724.0
  ]
  edge [
    source 38
    target 44
    key 0
    weight 423412.0
  ]
  edge [
    source 38
    target 73
    key 0
    weight 641.0
  ]
  edge [
    source 38
    target 56
    key 0
    weight 875.0
  ]
  edge [
    source 38
    target 0
    key 0
    weight 4189.0
  ]
  edge [
    source 38
    target 79
    key 0
    weight 4831.0
  ]
  edge [
    source 38
    target 26
    key 0
    weight 6180.0
  ]
  edge [
    source 38
    target 33
    key 0
    weight 27655.0
  ]
  edge [
    source 38
    target 41
    key 0
    weight 9189.0
  ]
  edge [
    source 38
    target 57
    key 0
    weight 2477.0
  ]
  edge [
    source 38
    target 72
    key 0
    weight 749039.0
  ]
  edge [
    source 38
    target 39
    key 0
    weight 1273.0
  ]
  edge [
    source 38
    target 5
    key 0
    weight 4056.0
  ]
  edge [
    source 38
    target 4
    key 0
    weight 293.0
  ]
  edge [
    source 38
    target 67
    key 0
    weight 24555.0
  ]
  edge [
    source 38
    target 18
    key 0
    weight 6349.0
  ]
  edge [
    source 40
    target 11
    key 0
    weight 4326.0
  ]
  edge [
    source 40
    target 30
    key 0
    weight 65315.0
  ]
  edge [
    source 40
    target 68
    key 0
    weight 20135.0
  ]
  edge [
    source 40
    target 12
    key 0
    weight 127810.0
  ]
  edge [
    source 40
    target 69
    key 0
    weight 6441.0
  ]
  edge [
    source 40
    target 10
    key 0
    weight 53800.0
  ]
  edge [
    source 40
    target 34
    key 0
    weight 41827.0
  ]
  edge [
    source 40
    target 54
    key 0
    weight 15469.0
  ]
  edge [
    source 40
    target 77
    key 0
    weight 571623.0
  ]
  edge [
    source 40
    target 25
    key 0
    weight 145719.0
  ]
  edge [
    source 40
    target 49
    key 0
    weight 45349.0
  ]
  edge [
    source 40
    target 50
    key 0
    weight 6472.0
  ]
  edge [
    source 40
    target 38
    key 0
    weight 390581.0
  ]
  edge [
    source 40
    target 2
    key 0
    weight 45517.0
  ]
  edge [
    source 40
    target 58
    key 0
    weight 17031.0
  ]
  edge [
    source 40
    target 74
    key 0
    weight 1446.0
  ]
  edge [
    source 40
    target 75
    key 0
    weight 6336.0
  ]
  edge [
    source 40
    target 65
    key 0
    weight 50098.0
  ]
  edge [
    source 40
    target 36
    key 0
    weight 5822.0
  ]
  edge [
    source 40
    target 48
    key 0
    weight 2225.0
  ]
  edge [
    source 40
    target 44
    key 0
    weight 31255.0
  ]
  edge [
    source 40
    target 0
    key 0
    weight 3156.0
  ]
  edge [
    source 40
    target 33
    key 0
    weight 9808.0
  ]
  edge [
    source 40
    target 41
    key 0
    weight 2819.0
  ]
  edge [
    source 40
    target 72
    key 0
    weight 56160.0
  ]
  edge [
    source 40
    target 39
    key 0
    weight 1841.0
  ]
  edge [
    source 40
    target 5
    key 0
    weight 1121.0
  ]
  edge [
    source 40
    target 67
    key 0
    weight 17826.0
  ]
  edge [
    source 44
    target 69
    key 0
    weight 3903.0
  ]
  edge [
    source 44
    target 34
    key 0
    weight 7993.0
  ]
  edge [
    source 44
    target 21
    key 0
    weight 385.0
  ]
  edge [
    source 44
    target 38
    key 0
    weight 30051.0
  ]
  edge [
    source 44
    target 2
    key 0
    weight 18508.0
  ]
  edge [
    source 44
    target 58
    key 0
    weight 5027.0
  ]
  edge [
    source 44
    target 65
    key 0
    weight 260968.0
  ]
  edge [
    source 44
    target 46
    key 0
    weight 995.0
  ]
  edge [
    source 44
    target 72
    key 0
    weight 15889.0
  ]
  edge [
    source 47
    target 10
    key 0
    weight 48804.0
  ]
  edge [
    source 47
    target 77
    key 0
    weight 959462.0
  ]
  edge [
    source 47
    target 7
    key 0
    weight 848.0
  ]
  edge [
    source 47
    target 51
    key 0
    weight 1289.0
  ]
  edge [
    source 47
    target 13
    key 0
    weight 2684.0
  ]
  edge [
    source 47
    target 28
    key 0
    weight 9952.0
  ]
  edge [
    source 47
    target 29
    key 0
    weight 2636.0
  ]
  edge [
    source 47
    target 20
    key 0
    weight 3868.0
  ]
  edge [
    source 47
    target 55
    key 0
    weight 2161.0
  ]
  edge [
    source 47
    target 79
    key 0
    weight 2373.0
  ]
  edge [
    source 47
    target 57
    key 0
    weight 3450.0
  ]
  edge [
    source 47
    target 18
    key 0
    weight 1565.0
  ]
  edge [
    source 49
    target 52
    key 0
    weight 42223.0
  ]
  edge [
    source 49
    target 52
    key 1
    weight 18272.0
  ]
  edge [
    source 49
    target 23
    key 0
    weight 35342.0
  ]
  edge [
    source 49
    target 23
    key 1
    weight 12512.0
  ]
  edge [
    source 49
    target 22
    key 0
    weight 15373.0
  ]
  edge [
    source 49
    target 76
    key 0
    weight 230744.0
  ]
  edge [
    source 49
    target 68
    key 0
    weight 38204.0
  ]
  edge [
    source 49
    target 69
    key 0
    weight 916.0
  ]
  edge [
    source 49
    target 71
    key 0
    weight 53919.0
  ]
  edge [
    source 49
    target 31
    key 0
    weight 10503.0
  ]
  edge [
    source 49
    target 70
    key 0
    weight 55480.0
  ]
  edge [
    source 49
    target 32
    key 0
    weight 4373.0
  ]
  edge [
    source 49
    target 34
    key 0
    weight 39619.0
  ]
  edge [
    source 49
    target 54
    key 0
    weight 5752.0
  ]
  edge [
    source 49
    target 17
    key 0
    weight 48335.0
  ]
  edge [
    source 49
    target 25
    key 0
    weight 653983.0
  ]
  edge [
    source 49
    target 15
    key 0
    weight 1194.0
  ]
  edge [
    source 49
    target 6
    key 0
    weight 587725.0
  ]
  edge [
    source 49
    target 37
    key 0
    weight 111298.0
  ]
  edge [
    source 49
    target 58
    key 0
    weight 4305.0
  ]
  edge [
    source 49
    target 42
    key 0
    weight 506.0
  ]
  edge [
    source 49
    target 74
    key 0
    weight 1724.0
  ]
  edge [
    source 49
    target 75
    key 0
    weight 7270.0
  ]
  edge [
    source 49
    target 65
    key 0
    weight 23651.0
  ]
  edge [
    source 49
    target 43
    key 0
    weight 393.0
  ]
  edge [
    source 49
    target 35
    key 0
    weight 10567.0
  ]
  edge [
    source 49
    target 46
    key 0
    weight 592.0
  ]
  edge [
    source 49
    target 1
    key 0
    weight 6932.0
  ]
  edge [
    source 49
    target 53
    key 0
    weight 1742.0
  ]
  edge [
    source 49
    target 19
    key 0
    weight 5832.0
  ]
  edge [
    source 49
    target 36
    key 0
    weight 13670.0
  ]
  edge [
    source 49
    target 48
    key 0
    weight 1475.0
  ]
  edge [
    source 49
    target 9
    key 0
    weight 6310.0
  ]
  edge [
    source 49
    target 44
    key 0
    weight 12909.0
  ]
  edge [
    source 49
    target 60
    key 0
    weight 19173.0
  ]
  edge [
    source 49
    target 66
    key 0
    weight 2815.0
  ]
  edge [
    source 49
    target 79
    key 0
    weight 2187.0
  ]
  edge [
    source 49
    target 16
    key 0
    weight 9265.0
  ]
  edge [
    source 49
    target 26
    key 0
    weight 8786.0
  ]
  edge [
    source 49
    target 33
    key 0
    weight 3059.0
  ]
  edge [
    source 49
    target 59
    key 0
    weight 11309.0
  ]
  edge [
    source 49
    target 63
    key 0
    weight 1929.0
  ]
  edge [
    source 49
    target 39
    key 0
    weight 1041.0
  ]
  edge [
    source 49
    target 3
    key 0
    weight 41195.0
  ]
  edge [
    source 49
    target 14
    key 0
    weight 1352.0
  ]
  edge [
    source 49
    target 67
    key 0
    weight 8530.0
  ]
  edge [
    source 50
    target 21
    key 0
    weight 6253.0
  ]
  edge [
    source 50
    target 2
    key 0
    weight 91691.0
  ]
  edge [
    source 52
    target 22
    key 0
    weight 15224.0
  ]
  edge [
    source 52
    target 70
    key 0
    weight 83159.0
  ]
  edge [
    source 52
    target 32
    key 0
    weight 4907.0
  ]
  edge [
    source 52
    target 17
    key 0
    weight 27963.0
  ]
  edge [
    source 52
    target 64
    key 0
    weight 197.0
  ]
  edge [
    source 55
    target 51
    key 0
    weight 787.0
  ]
  edge [
    source 55
    target 28
    key 0
    weight 1629.0
  ]
  edge [
    source 55
    target 29
    key 0
    weight 755.0
  ]
  edge [
    source 55
    target 20
    key 0
    weight 710.0
  ]
  edge [
    source 55
    target 18
    key 0
    weight 1223.0
  ]
  edge [
    source 57
    target 43
    key 0
    weight 772.0
  ]
  edge [
    source 57
    target 28
    key 0
    weight 906.0
  ]
  edge [
    source 57
    target 8
    key 0
    weight 3397.0
  ]
  edge [
    source 59
    target 70
    key 0
    weight 18936.0
  ]
  edge [
    source 59
    target 17
    key 0
    weight 23826.0
  ]
  edge [
    source 59
    target 25
    key 0
    weight 360130.0
  ]
  edge [
    source 59
    target 42
    key 0
    weight 537.0
  ]
  edge [
    source 59
    target 16
    key 0
    weight 12719.0
  ]
  edge [
    source 60
    target 68
    key 0
    weight 101038.0
  ]
  edge [
    source 60
    target 70
    key 0
    weight 15812.0
  ]
  edge [
    source 60
    target 36
    key 0
    weight 5754.0
  ]
  edge [
    source 60
    target 48
    key 0
    weight 2051.0
  ]
  edge [
    source 61
    target 63
    key 0
    weight 1670.0
  ]
  edge [
    source 63
    target 61
    key 0
    weight 514.0
  ]
  edge [
    source 63
    target 39
    key 0
    weight 994.0
  ]
  edge [
    source 65
    target 30
    key 0
    weight 55176.0
  ]
  edge [
    source 65
    target 12
    key 0
    weight 22469.0
  ]
  edge [
    source 65
    target 69
    key 0
    weight 12253.0
  ]
  edge [
    source 65
    target 34
    key 0
    weight 39206.0
  ]
  edge [
    source 65
    target 54
    key 0
    weight 2588.0
  ]
  edge [
    source 65
    target 21
    key 0
    weight 1297.0
  ]
  edge [
    source 65
    target 50
    key 0
    weight 3027.0
  ]
  edge [
    source 65
    target 38
    key 0
    weight 31011.0
  ]
  edge [
    source 65
    target 2
    key 0
    weight 19856.0
  ]
  edge [
    source 65
    target 58
    key 0
    weight 21744.0
  ]
  edge [
    source 65
    target 46
    key 0
    weight 1181.0
  ]
  edge [
    source 65
    target 53
    key 0
    weight 1173.0
  ]
  edge [
    source 65
    target 44
    key 0
    weight 223382.0
  ]
  edge [
    source 65
    target 33
    key 0
    weight 8861.0
  ]
  edge [
    source 65
    target 72
    key 0
    weight 81685.0
  ]
  edge [
    source 65
    target 64
    key 0
    weight 1692.0
  ]
  edge [
    source 65
    target 5
    key 0
    weight 833.0
  ]
  edge [
    source 66
    target 31
    key 0
    weight 4682.0
  ]
  edge [
    source 66
    target 25
    key 0
    weight 98586.0
  ]
  edge [
    source 66
    target 37
    key 0
    weight 48996.0
  ]
  edge [
    source 66
    target 3
    key 0
    weight 25786.0
  ]
  edge [
    source 66
    target 14
    key 0
    weight 32529.0
  ]
  edge [
    source 67
    target 62
    key 0
    weight 2566.0
  ]
  edge [
    source 67
    target 43
    key 0
    weight 330.0
  ]
  edge [
    source 67
    target 46
    key 0
    weight 6805.0
  ]
  edge [
    source 67
    target 36
    key 0
    weight 11562.0
  ]
  edge [
    source 67
    target 64
    key 0
    weight 2833.0
  ]
  edge [
    source 68
    target 23
    key 0
    weight 16791.0
  ]
  edge [
    source 68
    target 11
    key 0
    weight 21783.0
  ]
  edge [
    source 68
    target 22
    key 0
    weight 9082.0
  ]
  edge [
    source 68
    target 76
    key 0
    weight 84460.0
  ]
  edge [
    source 68
    target 31
    key 0
    weight 6014.0
  ]
  edge [
    source 68
    target 25
    key 0
    weight 169826.0
  ]
  edge [
    source 68
    target 15
    key 0
    weight 2920.0
  ]
  edge [
    source 68
    target 6
    key 0
    weight 40454.0
  ]
  edge [
    source 68
    target 37
    key 0
    weight 104241.0
  ]
  edge [
    source 68
    target 47
    key 0
    weight 43041.0
  ]
  edge [
    source 68
    target 58
    key 0
    weight 6023.0
  ]
  edge [
    source 68
    target 74
    key 0
    weight 4650.0
  ]
  edge [
    source 68
    target 75
    key 0
    weight 5510.0
  ]
  edge [
    source 68
    target 7
    key 0
    weight 558.0
  ]
  edge [
    source 68
    target 51
    key 0
    weight 2337.0
  ]
  edge [
    source 68
    target 1
    key 0
    weight 20995.0
  ]
  edge [
    source 68
    target 78
    key 0
    weight 3479.0
  ]
  edge [
    source 68
    target 13
    key 0
    weight 6121.0
  ]
  edge [
    source 68
    target 19
    key 0
    weight 3837.0
  ]
  edge [
    source 68
    target 29
    key 0
    weight 635.0
  ]
  edge [
    source 68
    target 36
    key 0
    weight 10859.0
  ]
  edge [
    source 68
    target 48
    key 0
    weight 24253.0
  ]
  edge [
    source 68
    target 9
    key 0
    weight 4659.0
  ]
  edge [
    source 68
    target 20
    key 0
    weight 1405.0
  ]
  edge [
    source 68
    target 8
    key 0
    weight 1528.0
  ]
  edge [
    source 68
    target 56
    key 0
    weight 1643.0
  ]
  edge [
    source 68
    target 60
    key 0
    weight 132214.0
  ]
  edge [
    source 68
    target 0
    key 0
    weight 8017.0
  ]
  edge [
    source 68
    target 79
    key 0
    weight 6272.0
  ]
  edge [
    source 68
    target 26
    key 0
    weight 12263.0
  ]
  edge [
    source 68
    target 41
    key 0
    weight 3123.0
  ]
  edge [
    source 68
    target 59
    key 0
    weight 8747.0
  ]
  edge [
    source 68
    target 57
    key 0
    weight 3536.0
  ]
  edge [
    source 68
    target 39
    key 0
    weight 2505.0
  ]
  edge [
    source 68
    target 67
    key 0
    weight 9174.0
  ]
  edge [
    source 70
    target 52
    key 0
    weight 206891.0
  ]
  edge [
    source 70
    target 23
    key 0
    weight 6898.0
  ]
  edge [
    source 70
    target 11
    key 0
    weight 6484.0
  ]
  edge [
    source 70
    target 22
    key 0
    weight 105299.0
  ]
  edge [
    source 70
    target 76
    key 0
    weight 131895.0
  ]
  edge [
    source 70
    target 68
    key 0
    weight 23565.0
  ]
  edge [
    source 70
    target 31
    key 0
    weight 13221.0
  ]
  edge [
    source 70
    target 32
    key 0
    weight 6599.0
  ]
  edge [
    source 70
    target 17
    key 0
    weight 167793.0
  ]
  edge [
    source 70
    target 77
    key 0
    weight 153074.0
  ]
  edge [
    source 70
    target 25
    key 0
    weight 251479.0
  ]
  edge [
    source 70
    target 49
    key 0
    weight 149173.0
  ]
  edge [
    source 70
    target 15
    key 0
    weight 1241.0
  ]
  edge [
    source 70
    target 50
    key 0
    weight 5164.0
  ]
  edge [
    source 70
    target 6
    key 0
    weight 43088.0
  ]
  edge [
    source 70
    target 38
    key 0
    weight 59827.0
  ]
  edge [
    source 70
    target 37
    key 0
    weight 30136.0
  ]
  edge [
    source 70
    target 2
    key 0
    weight 32204.0
  ]
  edge [
    source 70
    target 42
    key 0
    weight 2195.0
  ]
  edge [
    source 70
    target 75
    key 0
    weight 5582.0
  ]
  edge [
    source 70
    target 65
    key 0
    weight 33275.0
  ]
  edge [
    source 70
    target 40
    key 0
    weight 28814.0
  ]
  edge [
    source 70
    target 43
    key 0
    weight 234.0
  ]
  edge [
    source 70
    target 1
    key 0
    weight 10327.0
  ]
  edge [
    source 70
    target 13
    key 0
    weight 2865.0
  ]
  edge [
    source 70
    target 48
    key 0
    weight 1852.0
  ]
  edge [
    source 70
    target 9
    key 0
    weight 15059.0
  ]
  edge [
    source 70
    target 8
    key 0
    weight 575.0
  ]
  edge [
    source 70
    target 55
    key 0
    weight 861.0
  ]
  edge [
    source 70
    target 66
    key 0
    weight 2867.0
  ]
  edge [
    source 70
    target 79
    key 0
    weight 3286.0
  ]
  edge [
    source 70
    target 16
    key 0
    weight 7890.0
  ]
  edge [
    source 70
    target 33
    key 0
    weight 4122.0
  ]
  edge [
    source 70
    target 59
    key 0
    weight 21084.0
  ]
  edge [
    source 70
    target 63
    key 0
    weight 2826.0
  ]
  edge [
    source 70
    target 57
    key 0
    weight 6509.0
  ]
  edge [
    source 70
    target 3
    key 0
    weight 22120.0
  ]
  edge [
    source 70
    target 14
    key 0
    weight 2321.0
  ]
  edge [
    source 70
    target 67
    key 0
    weight 10219.0
  ]
  edge [
    source 70
    target 18
    key 0
    weight 1308.0
  ]
  edge [
    source 71
    target 11
    key 0
    weight 3572.0
  ]
  edge [
    source 71
    target 22
    key 0
    weight 13425.0
  ]
  edge [
    source 71
    target 76
    key 0
    weight 160542.0
  ]
  edge [
    source 71
    target 68
    key 0
    weight 31176.0
  ]
  edge [
    source 71
    target 31
    key 0
    weight 8937.0
  ]
  edge [
    source 71
    target 70
    key 0
    weight 45523.0
  ]
  edge [
    source 71
    target 17
    key 0
    weight 24383.0
  ]
  edge [
    source 71
    target 77
    key 0
    weight 221690.0
  ]
  edge [
    source 71
    target 25
    key 0
    weight 854660.0
  ]
  edge [
    source 71
    target 49
    key 0
    weight 71833.0
  ]
  edge [
    source 71
    target 50
    key 0
    weight 3937.0
  ]
  edge [
    source 71
    target 6
    key 0
    weight 61176.0
  ]
  edge [
    source 71
    target 38
    key 0
    weight 35681.0
  ]
  edge [
    source 71
    target 37
    key 0
    weight 130179.0
  ]
  edge [
    source 71
    target 2
    key 0
    weight 20560.0
  ]
  edge [
    source 71
    target 75
    key 0
    weight 6227.0
  ]
  edge [
    source 71
    target 7
    key 0
    weight 254.0
  ]
  edge [
    source 71
    target 43
    key 0
    weight 367.0
  ]
  edge [
    source 71
    target 35
    key 0
    weight 5246.0
  ]
  edge [
    source 71
    target 46
    key 0
    weight 450.0
  ]
  edge [
    source 71
    target 19
    key 0
    weight 4881.0
  ]
  edge [
    source 71
    target 36
    key 0
    weight 39365.0
  ]
  edge [
    source 71
    target 48
    key 0
    weight 1596.0
  ]
  edge [
    source 71
    target 9
    key 0
    weight 7603.0
  ]
  edge [
    source 71
    target 44
    key 0
    weight 14390.0
  ]
  edge [
    source 71
    target 60
    key 0
    weight 10241.0
  ]
  edge [
    source 71
    target 66
    key 0
    weight 3297.0
  ]
  edge [
    source 71
    target 16
    key 0
    weight 6366.0
  ]
  edge [
    source 71
    target 26
    key 0
    weight 5633.0
  ]
  edge [
    source 71
    target 33
    key 0
    weight 4232.0
  ]
  edge [
    source 71
    target 59
    key 0
    weight 6396.0
  ]
  edge [
    source 71
    target 63
    key 0
    weight 1218.0
  ]
  edge [
    source 71
    target 3
    key 0
    weight 130434.0
  ]
  edge [
    source 71
    target 14
    key 0
    weight 3283.0
  ]
  edge [
    source 71
    target 67
    key 0
    weight 11709.0
  ]
  edge [
    source 72
    target 69
    key 0
    weight 4364.0
  ]
  edge [
    source 72
    target 34
    key 0
    weight 7799.0
  ]
  edge [
    source 72
    target 21
    key 0
    weight 756.0
  ]
  edge [
    source 72
    target 38
    key 0
    weight 145781.0
  ]
  edge [
    source 72
    target 2
    key 0
    weight 14377.0
  ]
  edge [
    source 72
    target 65
    key 0
    weight 26035.0
  ]
  edge [
    source 72
    target 44
    key 0
    weight 22074.0
  ]
  edge [
    source 72
    target 64
    key 0
    weight 3179.0
  ]
  edge [
    source 73
    target 7
    key 0
    weight 151.0
  ]
  edge [
    source 73
    target 4
    key 0
    weight 1061.0
  ]
  edge [
    source 74
    target 48
    key 0
    weight 1849.0
  ]
  edge [
    source 74
    target 0
    key 0
    weight 6283.0
  ]
  edge [
    source 75
    target 74
    key 0
    weight 6054.0
  ]
  edge [
    source 75
    target 0
    key 0
    weight 7761.0
  ]
  edge [
    source 75
    target 26
    key 0
    weight 4621.0
  ]
  edge [
    source 75
    target 63
    key 0
    weight 1598.0
  ]
  edge [
    source 75
    target 39
    key 0
    weight 1640.0
  ]
  edge [
    source 76
    target 52
    key 0
    weight 410961.0
  ]
  edge [
    source 76
    target 23
    key 0
    weight 17678.0
  ]
  edge [
    source 76
    target 11
    key 0
    weight 11294.0
  ]
  edge [
    source 76
    target 30
    key 0
    weight 190501.0
  ]
  edge [
    source 76
    target 22
    key 0
    weight 36078.0
  ]
  edge [
    source 76
    target 68
    key 0
    weight 123163.0
  ]
  edge [
    source 76
    target 12
    key 0
    weight 45013.0
  ]
  edge [
    source 76
    target 69
    key 0
    weight 4798.0
  ]
  edge [
    source 76
    target 71
    key 0
    weight 46636.0
  ]
  edge [
    source 76
    target 31
    key 0
    weight 16104.0
  ]
  edge [
    source 76
    target 70
    key 0
    weight 124480.0
  ]
  edge [
    source 76
    target 10
    key 0
    weight 78570.0
  ]
  edge [
    source 76
    target 32
    key 0
    weight 9822.0
  ]
  edge [
    source 76
    target 34
    key 0
    weight 13811.0
  ]
  edge [
    source 76
    target 54
    key 0
    weight 15832.0
  ]
  edge [
    source 76
    target 17
    key 0
    weight 59006.0
  ]
  edge [
    source 76
    target 21
    key 0
    weight 1066.0
  ]
  edge [
    source 76
    target 77
    key 0
    weight 355999.0
  ]
  edge [
    source 76
    target 25
    key 0
    weight 399199.0
  ]
  edge [
    source 76
    target 49
    key 0
    weight 199080.0
  ]
  edge [
    source 76
    target 15
    key 0
    weight 16821.0
  ]
  edge [
    source 76
    target 50
    key 0
    weight 22291.0
  ]
  edge [
    source 76
    target 6
    key 0
    weight 109144.0
  ]
  edge [
    source 76
    target 38
    key 0
    weight 67967.0
  ]
  edge [
    source 76
    target 37
    key 0
    weight 177880.0
  ]
  edge [
    source 76
    target 2
    key 0
    weight 82737.0
  ]
  edge [
    source 76
    target 58
    key 0
    weight 7705.0
  ]
  edge [
    source 76
    target 42
    key 0
    weight 397.0
  ]
  edge [
    source 76
    target 74
    key 0
    weight 2560.0
  ]
  edge [
    source 76
    target 75
    key 0
    weight 25310.0
  ]
  edge [
    source 76
    target 7
    key 0
    weight 1067.0
  ]
  edge [
    source 76
    target 65
    key 0
    weight 86549.0
  ]
  edge [
    source 76
    target 40
    key 0
    weight 108865.0
  ]
  edge [
    source 76
    target 43
    key 0
    weight 3550.0
  ]
  edge [
    source 76
    target 35
    key 0
    weight 260930.0
  ]
  edge [
    source 76
    target 46
    key 0
    weight 4971.0
  ]
  edge [
    source 76
    target 1
    key 0
    weight 9988.0
  ]
  edge [
    source 76
    target 53
    key 0
    weight 11803.0
  ]
  edge [
    source 76
    target 78
    key 0
    weight 984.0
  ]
  edge [
    source 76
    target 13
    key 0
    weight 7956.0
  ]
  edge [
    source 76
    target 19
    key 0
    weight 18523.0
  ]
  edge [
    source 76
    target 36
    key 0
    weight 31607.0
  ]
  edge [
    source 76
    target 48
    key 0
    weight 2161.0
  ]
  edge [
    source 76
    target 9
    key 0
    weight 9428.0
  ]
  edge [
    source 76
    target 44
    key 0
    weight 42323.0
  ]
  edge [
    source 76
    target 73
    key 0
    weight 4784.0
  ]
  edge [
    source 76
    target 60
    key 0
    weight 24761.0
  ]
  edge [
    source 76
    target 0
    key 0
    weight 6183.0
  ]
  edge [
    source 76
    target 79
    key 0
    weight 4397.0
  ]
  edge [
    source 76
    target 16
    key 0
    weight 12045.0
  ]
  edge [
    source 76
    target 26
    key 0
    weight 16174.0
  ]
  edge [
    source 76
    target 33
    key 0
    weight 19072.0
  ]
  edge [
    source 76
    target 41
    key 0
    weight 20171.0
  ]
  edge [
    source 76
    target 59
    key 0
    weight 15021.0
  ]
  edge [
    source 76
    target 63
    key 0
    weight 2250.0
  ]
  edge [
    source 76
    target 57
    key 0
    weight 2005.0
  ]
  edge [
    source 76
    target 72
    key 0
    weight 28510.0
  ]
  edge [
    source 76
    target 39
    key 0
    weight 3315.0
  ]
  edge [
    source 76
    target 3
    key 0
    weight 28793.0
  ]
  edge [
    source 76
    target 64
    key 0
    weight 2833.0
  ]
  edge [
    source 76
    target 14
    key 0
    weight 5563.0
  ]
  edge [
    source 76
    target 4
    key 0
    weight 3574.0
  ]
  edge [
    source 76
    target 67
    key 0
    weight 64027.0
  ]
  edge [
    source 77
    target 24
    key 0
    weight 556.0
  ]
  edge [
    source 77
    target 27
    key 0
    weight 2141.0
  ]
  edge [
    source 77
    target 52
    key 0
    weight 55418.0
  ]
  edge [
    source 77
    target 23
    key 0
    weight 5786.0
  ]
  edge [
    source 77
    target 11
    key 0
    weight 73311.0
  ]
  edge [
    source 77
    target 30
    key 0
    weight 152931.0
  ]
  edge [
    source 77
    target 22
    key 0
    weight 27014.0
  ]
  edge [
    source 77
    target 45
    key 0
    weight 1433.0
  ]
  edge [
    source 77
    target 76
    key 0
    weight 469992.0
  ]
  edge [
    source 77
    target 68
    key 0
    weight 65224.0
  ]
  edge [
    source 77
    target 12
    key 0
    weight 120410.0
  ]
  edge [
    source 77
    target 69
    key 0
    weight 2902.0
  ]
  edge [
    source 77
    target 71
    key 0
    weight 47707.0
  ]
  edge [
    source 77
    target 31
    key 0
    weight 7496.0
  ]
  edge [
    source 77
    target 70
    key 0
    weight 70568.0
  ]
  edge [
    source 77
    target 10
    key 0
    weight 3059235.0
  ]
  edge [
    source 77
    target 32
    key 0
    weight 2828.0
  ]
  edge [
    source 77
    target 34
    key 0
    weight 62471.0
  ]
  edge [
    source 77
    target 54
    key 0
    weight 4387.0
  ]
  edge [
    source 77
    target 17
    key 0
    weight 22108.0
  ]
  edge [
    source 77
    target 21
    key 0
    weight 800.0
  ]
  edge [
    source 77
    target 25
    key 0
    weight 422693.0
  ]
  edge [
    source 77
    target 49
    key 0
    weight 102783.0
  ]
  edge [
    source 77
    target 15
    key 0
    weight 2709.0
  ]
  edge [
    source 77
    target 50
    key 0
    weight 40786.0
  ]
  edge [
    source 77
    target 6
    key 0
    weight 94976.0
  ]
  edge [
    source 77
    target 38
    key 0
    weight 680292.0
  ]
  edge [
    source 77
    target 37
    key 0
    weight 113558.0
  ]
  edge [
    source 77
    target 2
    key 0
    weight 250407.0
  ]
  edge [
    source 77
    target 47
    key 0
    weight 2788249.0
  ]
  edge [
    source 77
    target 58
    key 0
    weight 103484.0
  ]
  edge [
    source 77
    target 74
    key 0
    weight 7717.0
  ]
  edge [
    source 77
    target 75
    key 0
    weight 19258.0
  ]
  edge [
    source 77
    target 7
    key 0
    weight 7062.0
  ]
  edge [
    source 77
    target 65
    key 0
    weight 202843.0
  ]
  edge [
    source 77
    target 40
    key 0
    weight 286198.0
  ]
  edge [
    source 77
    target 43
    key 0
    weight 786.0
  ]
  edge [
    source 77
    target 35
    key 0
    weight 71520.0
  ]
  edge [
    source 77
    target 46
    key 0
    weight 480.0
  ]
  edge [
    source 77
    target 51
    key 0
    weight 5598.0
  ]
  edge [
    source 77
    target 1
    key 0
    weight 147671.0
  ]
  edge [
    source 77
    target 53
    key 0
    weight 2786.0
  ]
  edge [
    source 77
    target 78
    key 0
    weight 4898.0
  ]
  edge [
    source 77
    target 13
    key 0
    weight 85382.0
  ]
  edge [
    source 77
    target 19
    key 0
    weight 33476.0
  ]
  edge [
    source 77
    target 28
    key 0
    weight 29274.0
  ]
  edge [
    source 77
    target 29
    key 0
    weight 17983.0
  ]
  edge [
    source 77
    target 36
    key 0
    weight 99026.0
  ]
  edge [
    source 77
    target 48
    key 0
    weight 1870.0
  ]
  edge [
    source 77
    target 9
    key 0
    weight 169859.0
  ]
  edge [
    source 77
    target 20
    key 0
    weight 21231.0
  ]
  edge [
    source 77
    target 8
    key 0
    weight 11062.0
  ]
  edge [
    source 77
    target 44
    key 0
    weight 76791.0
  ]
  edge [
    source 77
    target 73
    key 0
    weight 23795.0
  ]
  edge [
    source 77
    target 55
    key 0
    weight 33670.0
  ]
  edge [
    source 77
    target 56
    key 0
    weight 2808.0
  ]
  edge [
    source 77
    target 60
    key 0
    weight 5617.0
  ]
  edge [
    source 77
    target 0
    key 0
    weight 40151.0
  ]
  edge [
    source 77
    target 79
    key 0
    weight 93511.0
  ]
  edge [
    source 77
    target 26
    key 0
    weight 12213.0
  ]
  edge [
    source 77
    target 33
    key 0
    weight 29306.0
  ]
  edge [
    source 77
    target 41
    key 0
    weight 14070.0
  ]
  edge [
    source 77
    target 59
    key 0
    weight 9890.0
  ]
  edge [
    source 77
    target 57
    key 0
    weight 44921.0
  ]
  edge [
    source 77
    target 72
    key 0
    weight 181924.0
  ]
  edge [
    source 77
    target 39
    key 0
    weight 2619.0
  ]
  edge [
    source 77
    target 3
    key 0
    weight 37419.0
  ]
  edge [
    source 77
    target 5
    key 0
    weight 1195.0
  ]
  edge [
    source 77
    target 14
    key 0
    weight 1211.0
  ]
  edge [
    source 77
    target 4
    key 0
    weight 11210.0
  ]
  edge [
    source 77
    target 67
    key 0
    weight 64465.0
  ]
  edge [
    source 77
    target 18
    key 0
    weight 27854.0
  ]
  edge [
    source 79
    target 11
    key 0
    weight 7886.0
  ]
  edge [
    source 79
    target 51
    key 0
    weight 2994.0
  ]
  edge [
    source 79
    target 13
    key 0
    weight 23556.0
  ]
  edge [
    source 79
    target 28
    key 0
    weight 2330.0
  ]
  edge [
    source 79
    target 29
    key 0
    weight 489.0
  ]
  edge [
    source 79
    target 8
    key 0
    weight 1503.0
  ]
  edge [
    source 79
    target 73
    key 0
    weight 1401.0
  ]
  edge [
    source 79
    target 55
    key 0
    weight 3293.0
  ]
  edge [
    source 79
    target 56
    key 0
    weight 1486.0
  ]
  edge [
    source 79
    target 57
    key 0
    weight 10377.0
  ]
  edge [
    source 79
    target 18
    key 0
    weight 2837.0
  ]
]
