 ID: LL1_cond_auth_network_dataset
 Name: cond_auth_network
 Description: Condensed matter, authorship network
 License: BSD
 License Link: https://snap.stanford.edu/snap/license.html
 Source: Stanford Network Analysis Project (SNAP)
 Source Link: https://snap.stanford.edu/data/ca-CondMat.html
 Citation: @misc{snapnets, author = {Jure Leskovec and Andrej Krevl}, title = {{SNAP Datasets}: {Stanford} Large Network Dataset Collection}, howpublished = {\url{http://snap.stanford.edu/data}}, month=jun, year=2014} @article{leskovec2007graph, title={Graph evolution: Densification and shrinking diameters}, author={Leskovec, Jure and Kleinberg, Jon and Faloutsos, Christos}, journal={ACM transactions on Knowledge Discovery from Data (TKDD)}, volume={1}, number={1}, pages={2--es}, year={2007}, publisher={ACM New York, NY, USA}}
