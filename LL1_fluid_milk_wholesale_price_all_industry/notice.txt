 ID: LL1_fluid_milk_wholesale_price_all_industry_dataset
 Name: Fluid_milk_wholesale_price_all_grades_us_average_Industry
 Description: Fluid milk, wholesale price (all grades), U.S. average INDUSTRY 
 License: open
 License Link: https://forecasters.org/resources/time-series-data/
 Source: International Institute of Forecasters; M-3 Competition Data
 Source Link: https://forecasters.org/resources/time-series-data/m3-competition/
 Citation: @article{makridakis2000m3, title={The M3-Competition: results, conclusions and implications}, author={Makridakis, Spyros and Hibon, Michele}, journal={International journal of forecasting}, volume={16}, number={4}, pages={451--476}, year={2000}, publisher={Elsevier}}
